<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'first_name',
        'created_at',
        'last_name',
        'email',
        'city',
        'country',
        'job_title',
        'job_text' ,
        'blogimage1',
        'blogimage2',
        'avatarlink',
        'presentation'     
    ];
}
