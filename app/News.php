<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'id',
        'author',
        'title1',
        'title2',
        'text1',
        'text2',
        'img1',
        'img2'   
    ];
}
