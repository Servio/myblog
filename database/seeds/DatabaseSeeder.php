<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts')->insert([

            'last_name' => 'Rossi',
            'first_name' => 'Alessandro',
            'email' => 'alessandro.rossi@libero.it',
            'city' => 'Roma',
            'country' => 'Italy',
            'job_title' => 'Doctor',
            'job_text' => 'Il titolo di dottore è un titolo accademico. In alcuni Paesi è riferito solo a chi ha raggiunto il più alto grado di istruzione universitaria, cioè il dottorato di ricerca, in altri Paesi, come lItalia, viene rivolto anche a chi ha conseguito una laurea o ai praticanti la professione medica in generale',
            'blogimage1'=> 'http://www.imgcinemas.it/wp-content/uploads/2018/01/atmos-1.png',
            'blogimage2'=> 'https://www.coastlineintl.com/wp-content/uploads/2017/11/medical-img.jpg',
            'presentation' => 'Dopo aver conseguito la laurea in Tecniche di Radiologia Medica per Immagini e Radioterapia di Ferrara, ho migliorato le mie competenze lavorando come volontario presso il presidio sanitario per indigenti della Onlus SantAnna. Oltre allattività esecutiva di tecnico per la risonanza magnetica, gestivo e programmavo i vari piani di trattamento radioterapici. Questa esperienza mi ha dato tanto, mettendomi spesso alla prova: ho eseguito controlli sui parametri vitali dei degenti e ho dovuto gestire situazioni complesse, mantenendo il dialogo coi pazienti e i loro familiari.'
            ]);
     DB::table('news')->insert([

        'author' => 'From Wikipedia, the free encyclopedia',
        'title1' => 'FakeNews',
        'title2' => 'Medieval',
        'text1' => 'In the 13th century BC, Rameses the Great spread lies and propaganda portraying the Battle of Kadesh as a stunning victory for the Egyptians;',
        'text2' => 'In 1475, a fake news story in Trent claimed that the Jewish community had murdered a two-and-a-half-year-old Christian infant named Simonino.',
        'img1' => 'http://www.bcmpa.org.uk/media/477583/banner_drink.png',
        'img2' => 'https://www.snopes.com/tachyon/2018/03/water_bottles_feature.jpg?fit=1200,628'
 ]);
    }
}
