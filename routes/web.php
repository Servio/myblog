<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('accounts', 'AccountController');
Route::resource('accounts/news', 'NewsController');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
/*Route::get('/accounts', function () {
    return view::make('layout');
});*/
