
<html>
<header>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
<link rel="stylesheet" href="https://use.typekit.net/iul5qke.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="/accounts">
    <img src="https://istartblogging.com/wp-content/uploads/2016/03/absolut-logo-design-typography-300x62.png" class="d-inline-block align-top" alt="">
  </a>
</nav>
</header>
<body>
<div class="container">
    <a class="arrow-go-back" href="/accounts"><i class="fas fa-arrow-left"></i></a>
<div class="row">
 <div class="col-sm-6">
    <h1 class="blog-page-title">{{$account->first_name}} {{$account->last_name}}</h1>
    <img class="blog-page-main-img" src="{{$account->avatarlink}}">
</div>
<div class="col-6">
<div class="card blog-page-contact-card">
   <h2> Info</h2>
    <h3>Email: <a href="#">{{$account->email}}</a></h3>
    <h3>City: {{$account->city}}</h3>
    <h3>Country: {{$account->country}}</h3>
</div>
<img class="blog-page-secondary-img" src="{{$account->blogimage1}}">
</div>
</div>
<div class="card blog-page-card-presentation">
    <h2 class="blog-page-card-presentation-title"> Job: {{$account->job_title}}</h4>
    <h4 class="blog-page-card-presentation-text"> Job descrip: {{$account->job_text}}</h4>
    <h4 class="blog-page-card-presentation-pres"> Presentation : <br>{{$account->presentation}}</h4>
    <img src="{{$account->blogimage2}}">
</div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
