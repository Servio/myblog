<html>
<header>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
<link rel="stylesheet" href="https://use.typekit.net/iul5qke.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/accounts">
            <img src="https://istartblogging.com/wp-content/uploads/2016/03/absolut-logo-design-typography-300x62.png" class="d-inline-block align-top" alt="">
          </a>
    <form class="form-inline filter-bar">
      <input class="form-control" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success" type="submit">Search</button>
    </form>  
      <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">News</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Blogs</a>
            </li>
            <li class="nav-item" >
              <a class="nav-link" href="/admin">A.P.</a>
            </li>
          </ul>
</nav>
</header>
<body>

<div class="image-background-start">

</div>
<div class="container">
<!--sometext start-->
<div class="row intro-card">
  <div class="col-4">
    <h1 class="intro-card-title"><i class="fab fa-airbnb"></i>My blog</h1>
  </div>
  <div class="col-6 offset-1">
    <p class="intro-card-text">
    A blog (a truncation of "weblog")[1] is a discussion or informational website published on the World Wide Web consisting of discrete, often informal diary-style text entries (posts). Posts are typically displayed in reverse chronological order, so that the most recent post appears first, at the top of the web page. Until 2009, blogs were usually the work of a single individual,[citation needed] occasionally of a small group, and often covered a single subject or topic. In the 2010s, "multi-author blogs" (MABs) emerged, featuring the writing of multiple authors and sometimes professionally edited. MABs from newspapers, other media outlets, universities, think tanks, advocacy groups, and similar institutions account for an increasing quantity of blog traffic. The rise of Twitter and other "microblogging" systems helps integrate MABs and single-author blogs into the news media. Blog can also be used as a verb, meaning to maintain or add content to a blog.
    </p>
  </div> 
</div>
<!--some text finish-->
<div class="row">
@section('main')
@foreach($accounts as $account)
            <div class="col-10">
                <div class="card card-blog">
                    <div class="row">
                      <div class="col-4">
                    <img class="card-blog-img" src="{{$account-> avatarlink}}"   />
                  </div>
                    <div class="col-5">
                      <div class="card-blog-body">
                        <h2 class="card-blog-name">{{$account->first_name}}    {{$account->last_name}}</h2>
                        <h5>Job: {{$account->job_title}}</h5>
                        <h5>Email : {{$account->email}}</h5>
                        <h5>City : {{$account->city}} </h5>
                        <h5>Country: {{$account->country}} </h5>
                        <p class="card-blog-smalltext">Added at :{{$account->created_at}}</p>
                        <form action="{{route('accounts.show', $account->id)}}" method="GET">
                        <button class="btn btn-warning" type="submit">Read blog </button>
                      </form>
                        </div>
                      </div>
                      <div class="col-2">
                        <label class="card-blog-star">
                          <i class="fas fa-star"></i>
                        </label>
                      </div>
                    </div>
                </div>
              </div>
@endforeach
</div>
<!-- news cases-->
</div>
<div class="container">
   <!--sometext start-->
<div class="row intro-card">
  <div class="col-6 offset-1">
    <p class="intro-card-text">
    News is information about current events. This may be provided through many different media: word of mouth, printing, postal systems, broadcasting, electronic communication, or through the testimony of observers and witnesses to events.
    Common topics for news reports include war, government, politics, education, health, the environment, economy, business, fashion, and entertainment, as well as athletic events, quirky or unusual events. Government proclamations, concerning royal ceremonies, laws, taxes, public health, and criminals, have been dubbed news since ancient times. Humans exhibit a nearly universal desire to learn and share news, which they satisfy by talking to each other and sharing information. Technological and social developments, often driven by government communication and espionage networks, have increased the speed with which news can spread, as well as influenced its content. The genre of news as we know it today is closely associated with the newspaper, which originated in China as a court bulletin and spread, with paper and printing press, to Europe.
    </p>
  </div> 
  <div class="col-4">
    <h1 class="intro-card-title"><i class="fab fa-angellist"></i>News</h1>
  </div>
</div>
<!--some text finish-->
  <div  class="row">
    @foreach ($news as $new)
    <div class="col-5">
      <div class="card card-news">
       <h3 class="card-news-title">{{$new->title1}}</h3>
       <img class="card-news-img" src="{{$new->img2}}">
       <h6 class="card-news-author"><small>Author:{{$new->author}}</small></h6>
       <a class="card-news-link" href="{{route('news.show', $new->id)}}"> Read news case</a>
       <h6 class="card-news-date"><small>Added at:{{$new->created_at}}</small></h6>
      </div>
    </div>
    @endforeach
  </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/cef03e2529.js" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
