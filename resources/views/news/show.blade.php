
<html>
<header>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
<link rel="stylesheet" href="https://use.typekit.net/iul5qke.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="/accounts">
    <img src="https://istartblogging.com/wp-content/uploads/2016/03/absolut-logo-design-typography-300x62.png" class="d-inline-block align-top" alt="">
  </a>
</nav>
</header>
<body>
<div class="container">
    <a class="arrow-go-back" href="/accounts"><i class="fas fa-arrow-left"></i></a>
      <div class="card news-page-card">
        <h1 class="news-page-card-title">"{{$news->title1}}" <br> <small> {{$news->author}}</small></h1>
        <div class="row">
        <div  class="col-5 offset-1">
        <h5 class="news-page-card-text">{{$news->text1}}</h5>
      </div>
      <div  class="col-6">
          <img class="news-page-card-img" src="{{$news->img1}}">
        </div>
      </div >
      <h3 class="news-page-card-subt">{{$news->title2}}</h3> <br>
      <div class="row">
        <div class="col-6">
        <img class="news-page-card-img" src="{{$news->img2}}">
      </div>
      <div class="col-5 offset-1">
        <h5 class="news-page-card-text">{{$news->text2}}</h5>
      </div>
      </div>
      </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
